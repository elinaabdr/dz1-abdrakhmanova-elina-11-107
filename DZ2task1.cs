using System;

namespace DZ2task1
{
    class Program
    {
        static void Main(string[] args)
        {
			int urovni = 4;
			
            for (int i = urovni; i > 0; i -= 1)
			{
				for (int j = urovni - i; j < urovni; j += 1)
				{
					Console.Write(" ");
				}
				for (int j = 0; j < urovni - i; j += 1)
				{
					Console.Write(" " + "*");
				}
				Console.WriteLine();
			}
        }
    }
}
