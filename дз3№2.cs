using System;

namespace dz3
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int[,] array = new int[3, 5]
            {
                { 44,21,3,43,5},
                { 13,44,15,20,21},
                { 19,22,29,28,50}
            };


            int min = 10000000;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (min > array[i, j])
                    {
                        min = array[i, j];
                    }
                }
            }
            Console.WriteLine(min);
        }
    }
}