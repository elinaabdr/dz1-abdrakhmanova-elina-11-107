using System;

namespace dz5_2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите целое число: ");
            int numb = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Факториал введенного числа равен: "+ Factorial(numb));

        }
        static int Factorial(int n)
        {
            if (n == 0)
                return 1;
            else
                return n * Factorial(n - 1);
        }
    }
}