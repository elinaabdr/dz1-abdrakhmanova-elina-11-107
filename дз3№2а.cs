using System;

namespace dz3_2a
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int height = 6;
            int width = 3;
            int[,] array = new int[height, width];
            int begin = 1;

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    array[i, j] = begin++;
                    Console.Write(array[i, j] + "\t");
                }
                Console.WriteLine();
            }

        }
    }
}