using System;

namespace dz2_2b
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение x: ");
            double x = Convert.ToDouble(Console.ReadLine()) * Math.PI / 180;

            double result1 = Math.Sin(x);

            Console.WriteLine("Введите значение точности: ");
            double accuracy = Convert.ToDouble(Console.ReadLine());

            double result2 = 0;
            int n = 0;

            while ((result1 - result2) > accuracy)
            {
                result2 += (Math.Pow((-1), n) * Math.Pow(x, 2 * n + 1)) / Factorial(2 * n + 1);
                n += 1;
            }
            Console.WriteLine(result2);
        }

        static int Factorial(int n)
        {
            int result_f = 1;
            for (int i = n; i > 1; i--)
                result_f *= i;
            return result_f;
        }
    }
}
