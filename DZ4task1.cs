using System;

namespace DZ3task1
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Введите число: ");
			int num = Convert.ToInt32(Console.ReadLine());
			int reverse = 0;
			int remainder = 0;

			while (num > 0)
			{
				remainder = num % 10;
				reverse = reverse * 10 + remainder;
				num /= 10;

			}
			Console.WriteLine("Отзеркаленное число: " + reverse);
		}
	}
}