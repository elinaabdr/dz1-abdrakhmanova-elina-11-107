using System;

namespace class5
{
    class MainClass
    {
        static void Main()
        {
            Console.WriteLine("Введите число X: ");
            int numb = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Число Fibonacci под x: " + Fibonacci(numb));
			Console.ReadLine();
        }
        static int Fibonacci(int numb)
        {
            if (numb == 0)
                return 0;
            else if (numb == 1)
				return 1;
			else
                return Fibonacci(numb - 1) + Fibonacci(numb - 1);
        }
    }
}