using System;

namespace DZ2task1
{
	class Program
	{
        private static int z;

        static void Main()
		{
			Console.WriteLine("Высота елки: ");
			int x = Convert.ToInt32(Console.ReadLine());

			for (int i = 1; i <= x; i++)
			{
				for (int j = 1; j <= 2 * x - i; j++)
				{
					Console.Write(" ");
				}
				for (int z = 1; z <= 2 * i - 1; z++)
				{
					Console.Write("*");
				}
				Console.WriteLine();
			}
			for (int i = 1; i <= x; i++)
			{
				for (int j = 1; j <= x - i; j++)
				{
					Console.Write(" ");
				}
				for (int z = 1; z <= 2 * i - 1; z++)
				{
					Console.Write("*");
				}
				for (int empty = 2*x-1 ; empty >= 1; empty--)
				{
					Console.Write(" ");
				}
				for (int z = 1; z <= i* 2 - 1 ; z++)
				{
					Console.Write("*");
				}
				Console.WriteLine();
			}
		}
	}
}
