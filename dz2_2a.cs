using System;

namespace dz2_2a
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите значение x: ");
            double x = Convert.ToDouble(Console.ReadLine());

            double result1 = Math.Exp(x);

            Console.Write("Введите значение точности: ");
            double accuracy = Convert.ToDouble(Console.ReadLine());

            double result2 = 0;
            int n = 0;

            while ((result1 - result2) > accuracy)
            {
                result2 += Math.Pow(x, n) / Factorial(n);
                n += 1;
            }
            Console.WriteLine(result2);
        }
        static int Factorial(int n)
        {
            int result_f = 1;

            for (int i = n; i > 1; i--)
            {
                result_f *= i;
            }
            return result_f;
        }
    }
}