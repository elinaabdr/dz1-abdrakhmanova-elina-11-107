using System;

namespace dz5_1
{
    class MainClass
    {
        static void Main(string[] args)
        {
            int[] array = new int[] {1,2,3,5,6,7};

            int answer = Sum(array);

            Console.WriteLine(answer);
        }

        static int Sum(int[] array, int n = 0)//n - для того, чтобы идти по массиву
        {
            if (n >= array.Length)
                return 0;
            else
                return array[n] + Sum(array, n + 1);
        }
    }
}
